<?php
/*
   Project PHP GoogleCode Webhook
   (C) 2012 Scorcher24

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

    Scorcher24
    scorcher24@gmail.com
 */

 function display_googlecode_webhook()
{
	@require_once('settings.php');
	
	try
	{
		// Connect to database
		$db = new PDO("mysql:host=".$db_host.";dbname=".$db_dbname.";charset=UTF-8", $db_uname, $db_passwd);
		
		// Select error report mode
		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
		
		// Build SQL String, if we use a bound parameter on the tablename, it fails. Odd..
		$sql = 'SELECT * from ' . $table_name . ' ORDER BY revision DESC LIMIT :count';
		
		// Prepare the sql statement
		$stmt = $db->prepare($sql);

		// Bind parameters to the statement
		$stmt->bindParam(':count', $show_count, PDO::PARAM_INT);			
		
		// Execute our query
		$stmt->execute();
		
		// Get the data from MySQL
		$rows = $stmt->fetchAll();

		// Walk and display results
		foreach($rows as $row )
		{
			// Open Left Table
			echo '<table border="0" align="left">';
			echo '<tbody>';
			
			// Committer
			echo '<tr>';
			echo '<td>Commited by:</td>';
			echo '<td>'.$row['author'].'</td>';
			echo '</tr>';
			
			// Revision
			echo '<tr>';
			echo '<td>Revision Nr.:</td>';
			echo '<td>'.$row['revision'].'</td>';
			echo '</tr>';
			
			// URL
			echo '<tr>';
			echo '<td>Repository URL:</td>';
			echo '<td><a href="'.$row['url'].'" target="_blank">'.$row['url'].'</a></td>';
			echo '</tr>';
			
			// Date and Time
			echo '<tr>';
			echo '<td>Commited on:</td>';
			echo '<td>'.$row['datetime'].'</td>';
			echo '</tr>';			
						
			// Close Table
			echo '</tbody>';
			echo '</table>';	

			// Open Right Table
			echo '<table border="0" align="right">';
			echo '<tbody>';
			
			// Commit Message
			echo '<tr>';
			echo '<td>'.nl2br($row['message'], true).'</td>';
			echo '</tr>';			

			// Close Table
			echo '</tbody>';
			echo '</table>';
		}
	}
	catch( Exception $e)
	{
		echo($e->getMessage());
	}
}
