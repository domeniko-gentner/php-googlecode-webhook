<?php
/*
   Project PHP GoogleCode Webhook
   (C) 2012 Scorcher24

   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.

   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you
      must not claim that you wrote the original software. If you use
      this software in a product, an acknowledgment in the product
      documentation would be appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and
      must not be misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
      distribution.

    Scorcher24
    scorcher24@gmail.com
 */

//****************************************************
// Config
//****************************************************

// google code secret key
$google_code_secret_key = 'ADD YOUR SECRET KEY HERE';

// google secret info
$google_secret_info = 'HTTP_GOOGLE_CODE_PROJECT_HOSTING_HOOK_HMAC';

// Settings
@require_once('settings.php');

// HTTP Header
function sendHttpStatus($code, $text)
{
	header("HTTP/1.0 ".$code." ".$text);
	exit;
}

$google_info = '';
if(array_key_exists($google_secret_info, $_SERVER))
{
	$google_info = $_SERVER[$google_secret_info];
}
if('' == $google_info)
{
	sendHttpStatus(403, 'Forbidden');
}

// Read Data or die
$data = file_get_contents('php://input');
if ( false === $data )
{
	sendHttpStatus(403, 'Forbidden');
}

// Decode JSON2 or die if invalid
$json_result = json_decode($data);	
if ( null === $json_result )
{
	sendHttpStatus(403, 'Forbidden');
}

// Verify google secret key
$hash_key = hash_hmac('md5', $data, $google_code_secret_key); 	
if($hash_key !== $google_info)
{
	sendHttpStatus(403, 'Forbidden');
}

try
{	
	// Connect to database
	$db = new PDO("mysql:host=".$db_host.";dbname=".$db_dbname.";charset=UTF-8", $db_uname, $db_passwd);
	
	// Select error report mode
	$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
		
	
	// Walk results and insert into db
	foreach($json_result->revisions as $revision)
	{
		// Extract needed data
		$data = new stdClass();				
		$data->id 			= null;
		$data->url 			= "http://code.google.com/p/".$google_project_name."/source/detail?r=" . $revision->revision;
		$data->datetime 	= date('Y-m-d H:i:s', $revision->timestamp);
		$data->message 		= $revision->message;
		$data->author 		= $revision->author;				
		$data->revision 	= $revision->revision;
		
		//	Create SQL query
		$query = 'INSERT INTO '.$table_name.'(id, url, datetime, message, author, revision) VALUES (NULL, :url, :datetime, :message, :author, :revision)';
		
		// Prepare Statement
		$stmt = $db->prepare($query);
		
		// Bind Parameters
		$stmt->bindParam(':url', $data->url, PDO::PARAM_STR);
		$stmt->bindParam(':datetime', $data->datetime, PDO::PARAM_STR);
		$stmt->bindParam(':message', $data->message, PDO::PARAM_STR);
		$stmt->bindParam(':author', $data->author, PDO::PARAM_STR);
		$stmt->bindParam(':revision', $data->revision, PDO::PARAM_STR);
		
		// Execute
		$stmt->execute();
	}
	
	// Report success to google
	sendHttpStatus('200', 'Success');
}
catch( Exception $e)
{
	// Leave a file, so we can identify problems 
	$handle = fopen("myException.txt", "w");   
	fwrite($handle, $e->getMessage());   
	fclose($handle);  
	
	// Forbidden
	sendHttpStatus(403, 'Forbidden');
}


?>