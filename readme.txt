== Introduction ==

This is php code for using googlecode webhook on your own homepage. 
It outputs valid XHTML 1.0 compatible HTML with two tables that sit next to each other. 
But it is very easy to customize if you know HTML, just look at the display.php file. 

== Requirements ==

* PHP 5.1.0
* MySQL Database

== Usage == 

First, access your database and execute the googlecode_webhook.sql, it just adds the table schemata. You can rename the table at any time, but you have to change the $table_name in the file settings.php afterwards.

Upload the files to a directory of its own on your webspace. Edit settings.php with the values needed to access the database and your google code secret key in receiver.php.
Then upload it to your webspace and enter the URL on your projects admin section. 

I strongly advise to rename the files receiver.php and display.php to something totally random to make this script more secure.
If you need some randomness, use http://random.org.

You can use the code in any portal or cms you want. Just make sure they allow custom 
php such as SMF or XOOPS. Then just add this to the custom block:

@require_once($_SERVER['DOCUMENT_ROOT'] .  '/yourpath/display.php');
display_googlecode_webhook();


If you use SMF with SimplePortal:

global $boarddir;
@require_once($boarddir .  '/yourpath/display.php');
display_googlecode_webhook();


That should do the trick. You need to use the same database as your portal though.
I don't require any credits if you use this, but it would be nice if you could link to my homepage somewhere.
If you need an icon, contact me please :).
If you want to comment or need help, visit my homepage (where you can also see it in action) and ask on the forums :). 

Rock on \m/,
Scorcher24
scorcher24@gmail.com
http://nightlight2d.de